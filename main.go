package main

import (
	"os"

	"git.tars.one/ada/app"
	"git.tars.one/ada/resource/file"
	"git.tars.one/ada/resource/openai"
	"git.tars.one/ada/ui"
)

func main() {
	ctx := app.NewCtx()

	if app.Config.Profile {
		go profiler(ctx)
	}

	ai, err := openai.NewOpenAI(ctx)
	if err != nil {
		ctx.Log.Fatalf("Error initializing OpenAI client: %s\n", err)
		os.Exit(1)
	}

	fs, err := file.NewStorageExternal(app.DataDir)
	if err != nil {
		ctx.Log.Fatalf("Error initializing storage client: %s\n", err)
		os.Exit(1)
	}
	defer fs.Close()

	ui := ui.NewGUI(ctx, ai, fs)
	ui.Run(ctx)
}
