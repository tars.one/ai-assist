package app

import (
	c "context"

	"git.tars.one/atlas/context"
	"git.tars.one/atlas/logger"
	"github.com/google/uuid"
)

type AppCtx struct{}

type Ctx = context.Ctx[AppCtx]

func NewCtx() *Ctx {
	return &Ctx{
		Context:   c.TODO(),
		RequestID: uuid.New(),
		Log:       logger.NewLogrusLogger(),
	}
}
