package app

import (
	"os"
	"path"
)

var JSONConfigFile = ".config/ada/config.json"
var DataDir = ".local/ada/data"
var AssetsDir = path.Join(DataDir, "assets")
var Homedir string

func init() {
	dir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	Homedir = dir
	JSONConfigFile = path.Join(Homedir, JSONConfigFile)
	DataDir = path.Join(Homedir, DataDir)
}

type AppConfig struct {
	Profile bool `json-var:"app_profiler"`
}

var Config AppConfig
