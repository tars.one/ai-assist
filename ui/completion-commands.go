package ui

import (
	"fmt"
	"strconv"

	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
	"git.tars.one/atlas/logger"
)

func (a *UI) handleCompletionCommand(ctx *app.Ctx, command []string) bool {
	switch command[0] {
	case "/set":
		if len(command) < 3 {
			break
		}

		switch field, val := command[1], command[2]; field {
		case "#tokens":
			i, _ := strconv.ParseInt(val, 10, 64)
			a.cs.setNumTokens(ctx, int(i))
		}

	default:
		return false
	}

	return true
}

func (cs *completionState) setNumTokens(ctx *app.Ctx, n int) {
	if n < Config.MinTokens {
		cs.numTokens = Config.MinTokens
	} else if n > Config.MaxTokens {
		cs.numTokens = Config.MaxTokens
	} else {
		cs.numTokens = n
	}

	cs.r.addMessage(ctx, NewMessage(
		entity.MimeText,
		MsgKindSysInfo,
		fmt.Sprintf("set max_tokens => %d", cs.numTokens),
	))

	ctx.Log.WithFields(logger.Fields{
		"command": "/set",
		"#tokens": cs.numTokens,
	}).Info("updating state")
}
