package ui

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"

	"git.tars.one/atlas/logger"

	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
)

type msgRenderer struct {
	promptInput *widget.Entry
	loading     *widget.ProgressBarInfinite

	heading fyne.CanvasObject

	msgs []*entity.Message

	ui *UI
}

func newMsgRenderer(ctx *app.Ctx, ui *UI,
	heading, placeholder string,
	promptHanlder func(*entity.Message),
	cmdHandler uiCommandHandler,
) *msgRenderer {
	ret := &msgRenderer{
		heading: widget.NewRichTextFromMarkdown(heading),
		ui:      ui,
	}

	ret.loading = func() *widget.ProgressBarInfinite {
		loading := widget.NewProgressBarInfinite()
		loading.Hide()
		return loading
	}()

	ret.promptInput = func() *widget.Entry {
		entry := widget.NewMultiLineEntry()
		entry.Wrapping = fyne.TextWrapWord
		entry.OnSubmitted = func(input string) {
			entry.SetText("")
			if input == "" {
				return
			}
			if isCommand, err := ui.handleCommand(ctx, input, cmdHandler); err != nil || isCommand {
				return
			}

			msg := NewMessage(entity.MimeText, MsgKindUserPrompt, input)
			ret.addMessage(ctx, msg)

			entry.Disable()
			ret.loading.Show()
			defer func() {
				entry.Enable()
				ret.loading.Hide()
			}()

			promptHanlder(msg)
		}
		entry.SetPlaceHolder(placeholder)
		entry.Refresh()
		return entry
	}()
	return ret
}

const (
	MsgKindAICompletion = "ai-completion"
	MsgKindAIImage      = "ai-image"
	MsgKindUserPrompt   = "user"

	MsgKindSysError = "sys-error"
	MsgKindSysInfo  = "sys-state-update"
)

func NewMessage(m entity.Mime, kind, data string) *entity.Message {
	t := time.Now()
	return &entity.Message{
		Mime: m,
		Kind: kind,
		Data: data,
		Meta: entity.Metadata{
			CreatedAt: t,
			AppData: func() map[string]interface{} {
				ret := map[string]interface{}{}
				if kind == MsgKindUserPrompt {
					ret["prompt_id"] = time.Now().Format("p_01-02-2006_15:04:05")
				}
				return ret
			}(),
		},
	}
}

func (m *msgRenderer) addMessage(ctx *app.Ctx, msg *entity.Message) {
	if !strings.HasPrefix(msg.Kind, "sys") {
		if err := m.ui.storage.StoreMessage(ctx, msg); err != nil {
			ctx.Log.Error(err)
		}
	}

	m.msgs = append(m.msgs, msg)
	m.render(ctx)
}

func (m *msgRenderer) clearMessages() {
	m.msgs = nil
}

func (m *msgRenderer) render(ctx *app.Ctx) {
	log := ctx.Log.WithFields(logger.Fields{
		"component": "ui.msgRenderer",
		"function":  "render",
	})
	log.Info()

	messages := container.NewVBox()

	textMsg := func(text string) *widget.RichText {
		t := widget.NewRichTextFromMarkdown(text)
		t.Wrapping = fyne.TextWrapWord
		return t
	}

	for _, msg := range m.msgs {
		switch msg.Kind {
		case MsgKindUserPrompt:
			messages.Add(textMsg(fmt.Sprintf("* * *\n\n__User__: %s", msg.Data)))

		// image specific

		case MsgKindAIImage:
			index := msg.Meta.AppData["index"].(int)
			outOf := msg.Meta.AppData["out_of"].(int)

			raw, err := base64.StdEncoding.DecodeString(msg.Data)
			if err != nil {
				log.Errorf("error while converting from base64: %s", err)
				continue
			}

			messages.Add(textMsg(fmt.Sprintf("__Image %d of %d__", index+1, outOf)))

			img := canvas.NewImageFromReader(bytes.NewReader(raw), msg.Data)
			img.SetMinSize(fyne.NewSize(256, 256))

			messages.Add(img)

		// completion specific

		case MsgKindAICompletion:
			messages.Add(textMsg(fmt.Sprintf("__AI__: %s", msg.Data)))

		case MsgKindSysError:
			messages.Add(textMsg(fmt.Sprintf("__System Error__: `%s`", msg.Data)))

		case MsgKindSysInfo:
			messages.Add(textMsg(fmt.Sprintf("__System Info__: `%s`", msg.Data)))
		}
	}

	scroll := container.NewScroll(messages)
	scroll.SetMinSize(fyne.NewSize(300, 400))

	split := container.NewVSplit(
		container.NewVBox(
			m.heading,
			container.NewPadded(m.loading),
			scroll,
		),
		m.promptInput,
	)
	split.SetOffset(0.7)

	scroll.ScrollToBottom()
	scroll.Refresh()

	m.ui.setMainContent(split)
}
