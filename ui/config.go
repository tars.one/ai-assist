package ui

type UIConfig struct {
	AppName string `json-var:"app_name"`

	MinTokens int `json-var:"min_tokens"`
	MaxTokens int `json-var:"max_tokens"`
}

var Config UIConfig
