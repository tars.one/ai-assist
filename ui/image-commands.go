package ui

import (
	"fmt"
	"strconv"

	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
	"git.tars.one/ada/external"

	"git.tars.one/atlas/logger"
)

func (a *UI) handleImageCommand(ctx *app.Ctx, command []string) bool {
	switch command[0] {
	case "/set":
		if len(command) < 3 {
			break
		}

		switch field, val := command[1], command[2]; field {
		case "n":
			i, _ := strconv.ParseInt(val, 10, 64)
			a.is.setN(ctx, int(i))

		case "size":
			i, _ := strconv.ParseInt(val, 10, 64)
			a.is.setSize(ctx, int(i))
		}

	default:
		return false
	}

	return true
}

func (is *imageState) setN(ctx *app.Ctx, n int) {
	if n < 1 {
		is.n = 1
	} else if n > 5 {
		is.n = 5
	} else {
		is.n = n
	}

	is.r.addMessage(ctx, NewMessage(
		entity.MimeText,
		MsgKindSysInfo,
		fmt.Sprintf("set n => %d", is.n),
	))

	ctx.Log.WithFields(logger.Fields{
		"command": "/set",
		"n":       is.n,
	}).Info("updating state")
}

func (is *imageState) setSize(ctx *app.Ctx, n int) {

	switch n {
	case 256:
		is.size = external.ImgSize256
	case 512:
		is.size = external.ImgSize512
	case 1024:
		is.size = external.ImgSize1024
	default:
		is.r.addMessage(ctx, NewMessage(
			entity.MimeText,
			MsgKindSysError,
			fmt.Sprintf("size must be one of: 256, 512, 1024; got %d", n),
		))
		return
	}

	is.r.addMessage(ctx, NewMessage(
		entity.MimeText,
		MsgKindSysInfo,
		fmt.Sprintf("set size => %dx%d", n, n),
	))

	ctx.Log.WithFields(logger.Fields{
		"command": "/set",
		"size":    is.size,
	}).Info("updating state")
}
