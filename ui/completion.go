package ui

import (
	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
	"git.tars.one/ada/external"

	"git.tars.one/atlas/logger"
)

type completionState struct {
	r *msgRenderer

	h, w        float32
	numTokens   int
	splitOffset float64

	updateSize func(w, h float32)
}

func (a *UI) renderCompletionMode(ctx *app.Ctx) {
	log := ctx.Log.WithFields(logger.Fields{
		"component": "ui",
		"function":  "CompletionMode",
	})
	log.Info()

	if a.cs == nil {
		a.cs = &completionState{
			r: newMsgRenderer(ctx, a,
				"# Completions",
				"The heaviest element is",
				func(msg *entity.Message) { a.handlePromptSubmission(ctx, msg) },
				a.handleCompletionCommand,
			),
		}
	}

	a.activeRenderer = a.cs.r
	a.cs.r.render(ctx)
}

func (a *UI) handlePromptSubmission(ctx *app.Ctx, prompt *entity.Message) {
	req := external.CompletionRequest{
		Tokens: a.cs.numTokens,
		Prompt: prompt.Data,
	}
	comp, err := a.ext.Complete(ctx, req)
	if err != nil {
		a.cs.r.addMessage(ctx, NewMessage(
			entity.MimeText,
			MsgKindSysError,
			"There was an error while fetching completions!",
		))
	} else {
		m := NewMessage(entity.MimeText, MsgKindAICompletion, comp)
		m.Meta.AppData["prompt_id"] = prompt.Meta.AppData["prompt_id"]
		a.cs.r.addMessage(ctx, m)
	}
}
