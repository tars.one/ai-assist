package ui

import (
	"fmt"
	"path"

	"fyne.io/fyne/v2"
	ui "fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"

	"git.tars.one/ada/app"
	"git.tars.one/ada/external"
	"git.tars.one/atlas/logger"
)

type UI struct {
	ui   fyne.App
	wins []fyne.Window

	ext     external.UIExternal
	storage external.Storage

	theme string
	home  fyne.CanvasObject

	activeRenderer *msgRenderer

	cs *completionState
	is *imageState
}

func NewGUI(ctx *app.Ctx, ext external.UIExternal, store external.Storage) *UI {
	ui := ui.New()
	ui.Settings().SetTheme(theme.DarkTheme())

	// main window
	win := ui.NewWindow("AI Assist")
	win.SetFixedSize(true)
	win.SetPadded(true)
	win.SetMaster()

	gui := &UI{
		ui:      ui,
		wins:    []fyne.Window{win},
		ext:     ext,
		storage: store,
		theme:   "dark",
	}

	gui.renderHome(ctx)

	return gui
}

func (a *UI) renderHome(ctx *app.Ctx) {
	log := ctx.Log.WithFields(logger.Fields{
		"component": "ui",
		"function":  "renderHome",
	})
	log.Info("rendering")

	// If the home page has not been rendered before, then render it once, and
	// store the rendered object.
	if a.home == nil {
		// application icon
		icon := canvas.NewImageFromFile(path.Join(app.AssetsDir, "icon.ico"))
		icon.SetMinSize(fyne.NewSize(100, 100))
		icon.FillMode = canvas.ImageFillContain

		// section heading
		heading := container.NewPadded(container.NewCenter(container.NewHBox(
			container.NewPadded(icon),
			container.NewVBox(
				widget.NewRichTextFromMarkdown("# ADA"),
				widget.NewRichTextFromMarkdown("AI Utils"),
			),
		)))

		// actions section
		actions := container.NewPadded(container.NewVBox(
			widget.NewButtonWithIcon("Completion", theme.SearchIcon(),
				func() { a.renderCompletionMode(ctx) }),
			widget.NewButtonWithIcon("Image", theme.MediaPhotoIcon(),
				func() { a.renderImageMode(ctx) }),

			widget.NewSeparator(),

			widget.NewButtonWithIcon("Quit", theme.CancelIcon(),
				func() { a.ui.Quit() }),
		))

		// footer
		footer := container.NewPadded(container.NewCenter(
			widget.NewRichTextFromMarkdown(fmt.Sprintf(
				"__Made by__ %s | _Powered by_ %s.",
				"[tars.one](https://gitlab.com/tars.one/ada)",
				"[OpenAI](https://openai.com)",
			)),
		))

		homeContent := container.NewScroll(
			container.NewPadded(container.NewCenter(container.NewVBox(
				heading,
				actions,
				footer,
			))),
		)
		homeContent.SetMinSize(fyne.NewSize(300, 600))

		a.home = homeContent
	}

	// Set the cached pre-rendered canvas object for the home page.
	a.setMainContent(a.home)
}

func (a *UI) setMainContent(c fyne.CanvasObject) { a.wins[0].SetContent(c) }

func (a *UI) Run(ctx *app.Ctx) { a.wins[0].ShowAndRun() }
