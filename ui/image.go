package ui

import (
	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
	"git.tars.one/ada/external"
	"git.tars.one/atlas/logger"
)

type imageState struct {
	r *msgRenderer

	n    int
	size external.ImageSize
}

func (a *UI) renderImageMode(ctx *app.Ctx) {
	log := ctx.Log.WithFields(logger.Fields{
		"component": "ui",
		"function":  "ImageMode",
	})
	log.Info("rendering")

	if a.is == nil {
		a.is = &imageState{
			r: newMsgRenderer(ctx, a,
				"# Image Generation",
				"A photo of a white fur monster standing in a purple room",
				func(m *entity.Message) { a.handleImagePromptSubmission(ctx, m) },
				a.handleImageCommand,
			),
		}
	}

	a.activeRenderer = a.is.r
	a.is.r.render(ctx)
}

func (a *UI) handleImagePromptSubmission(ctx *app.Ctx, prompt *entity.Message) {
	resp, err := a.ext.Generate(ctx, external.ImageRequest{
		Prompt: prompt.Data,
		N:      a.is.n,
		Size:   a.is.size,
	})
	if err != nil {
		a.is.r.addMessage(ctx, NewMessage(
			entity.MimeText,
			MsgKindSysError,
			"There was an error while fetching image generations!",
		))
	}
	for i, raw := range resp {
		m := NewMessage(entity.MimeBase64PNG, MsgKindAIImage, raw)
		m.Meta.AppData["prompt_id"] = prompt.Meta.AppData["prompt_id"]
		m.Meta.AppData["index"] = i
		m.Meta.AppData["out_of"] = len(resp)
		a.is.r.addMessage(ctx, m)
	}
}
