package ui

import (
	"fmt"
	"strings"

	"fyne.io/fyne/v2/theme"
	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
	"git.tars.one/atlas/logger"
)

type uiCommandHandler func(ctx *app.Ctx, cmd []string) bool

func (a *UI) handleCommand(ctx *app.Ctx, text string, handler uiCommandHandler) (bool, error) {
	_ = ctx.Log.WithFields(logger.Fields{
		"component": "ui",
		"function":  "handleSystemCommand",
	})

	// All system commands begin with a forward slash.
	// We assume that the text has been stripped of whitespace, so it;s enough to
	// look for the slash in the first character.
	if !strings.HasPrefix(text, "/") {
		return false, nil
	}

	command := strings.Split(text, " ")
	switch action := command[0]; action {
	case "/quit":
		a.quit(ctx)

	case "/theme":
		a.toggleTheme(ctx)

	case "/home", "/back":
		a.renderHome(ctx)

	case "/clear", "/cls":
		a.activeRenderer.clearMessages()

	default:
		var validCommand bool
		if handler != nil {
			validCommand = handler(ctx, command)
		}
		if !validCommand {
			a.activeRenderer.addMessage(ctx, NewMessage(
				entity.MimeText,
				MsgKindSysError,
				fmt.Sprintf("unknown action: %s", action),
			))
			return false, fmt.Errorf("unknown action: %s", action)
		}
		return validCommand, nil
	}

	return true, nil
}

func (a *UI) quit(ctx *app.Ctx) {
	ctx.Log.WithFields(logger.Fields{
		"command": "/quit",
	}).Info("exiting GUI")

	a.ui.Quit()
}

func (a *UI) toggleTheme(ctx *app.Ctx) {
	ctx.Log.WithFields(logger.Fields{
		"component": "ui",
		"function":  "toggleTheme",
		"new_theme": a.theme,
	}).Info("toggling theme")

	if a.theme == "dark" {
		a.ui.Settings().SetTheme(theme.LightTheme())
		a.theme = "light"
	} else {
		a.ui.Settings().SetTheme(theme.DarkTheme())
		a.theme = "dark"
	}
	a.home = nil

	a.activeRenderer.addMessage(ctx, NewMessage(
		entity.MimeText,
		MsgKindSysInfo,
		"toggle theme",
	))
}
