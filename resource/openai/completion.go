package openai

import (
	"git.tars.one/ada/app"
	"git.tars.one/ada/external"
	"git.tars.one/atlas/logger"
	"github.com/sashabaranov/go-openai"
)

const (
	MinTokens = 50
	MaxTokens = 256
)

func sanitizeCompletionRequest(req external.CompletionRequest) external.CompletionRequest {
	if req.Tokens < MinTokens {
		req.Tokens = MinTokens
	} else if req.Tokens > MaxTokens {
		req.Tokens = MaxTokens
	}
	return req
}

func (o *OpenAI) Complete(ctx *app.Ctx, input external.CompletionRequest) (string, error) {
	input = sanitizeCompletionRequest(input)

	// form request, and call api
	req := openai.CompletionRequest{
		Model:       openai.GPT3TextDavinci003,
		Prompt:      input.Prompt,
		MaxTokens:   input.Tokens,
		Temperature: 0.7,
		N:           1,
	}
	log := ctx.Log.WithFields(logger.Fields{
		"component":   "resource/openai",
		"function":    "Complete",
		"model":       req.Model,
		"prompt":      req.Prompt,
		"max_tokens":  req.MaxTokens,
		"temperature": req.Temperature,
		"n":           req.N,
	})
	log.Info("fetching completion")

	resp, err := o.client.CreateCompletion(ctx.Context, req)
	if err != nil {
		log.Error(err)
		return "Sorry! An error occurred. Please try again later.", err
	}

	return resp.Choices[0].Text, nil
}
