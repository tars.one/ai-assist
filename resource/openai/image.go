package openai

import (
	"git.tars.one/ada/app"
	"git.tars.one/ada/external"
	"git.tars.one/atlas/logger"
	"github.com/sashabaranov/go-openai"
)

func sanitizeImageReq(req external.ImageRequest) external.ImageRequest {
	return req
}

func getSize(s external.ImageSize) string {
	switch s {
	case external.ImgSize1024:
		return openai.CreateImageSize1024x1024
	case external.ImgSize512:
		return openai.CreateImageSize512x512
	default:
		return openai.CreateImageSize256x256
	}
}

func (o *OpenAI) Generate(ctx *app.Ctx, req external.ImageRequest) ([]string, error) {
	size := getSize(req.Size)
	log := ctx.Log.WithFields(logger.Fields{
		"component": "resource/openai",
		"function":  "GenerateImage",
		"prompt":    req.Prompt,
		"n":         req.N,
		"size":      size,
	})
	log.Info("fetching images")

	req = sanitizeImageReq(req)

	resp, err := o.client.CreateImage(ctx.Context, openai.ImageRequest{
		Prompt:         req.Prompt,
		N:              req.N,
		Size:           size,
		ResponseFormat: openai.CreateImageResponseFormatB64JSON,
	})
	if err != nil {
		log.Error(err)
		return nil, err
	}

	var ret []string
	for _, r := range resp.Data {
		ret = append(ret, r.B64JSON)
	}

	return ret, nil
}
