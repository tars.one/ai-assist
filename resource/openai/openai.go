package openai

import (
	"fmt"

	"git.tars.one/ada/app"
	oai "github.com/sashabaranov/go-openai"
)

type OpenAI struct {
	client *oai.Client
}

func NewOpenAI(ctx *app.Ctx) (*OpenAI, error) {
	c := oai.NewClient(Config.ApiKey)
	if c == nil {
		err := fmt.Errorf("Failed to instantiate client")
		ctx.Log.Error(err)
		return nil, err
	}

	return &OpenAI{c}, nil
}
