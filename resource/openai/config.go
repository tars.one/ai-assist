package openai

type OpenAIConfig struct {
	ApiKey string `json-var:"openai_api_key"`
}

var Config OpenAIConfig
