package file

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"path"

	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"

	"git.tars.one/atlas/logger"
)

const (
	dataFile = "usage.json"
	imageDir = "images"
)

type StorageExternal struct {
	basedir string
	f       *os.File
}

func mkdir(dir string) error {
	if _, err := os.Stat(dir); err == nil {
		return nil
	} else if !os.IsNotExist(err) {
		return fmt.Errorf("error when creating directory '%s': %s", dir, err)
	}
	return os.MkdirAll(dir, os.ModePerm)
}

func NewStorageExternal(dirpath string) (*StorageExternal, error) {
	if err := mkdir(path.Join(dirpath, imageDir)); err != nil {
		return nil, err
	}

	file, err := os.OpenFile(path.Join(dirpath, dataFile),
		os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}

	return &StorageExternal{dirpath, file}, nil
}

func (e *StorageExternal) Close() error { return e.f.Close() }

func (e *StorageExternal) StoreMessage(ctx *app.Ctx, m *entity.Message) error {
	log := ctx.Log.WithFields(logger.Fields{
		"component": "StorageExternal",
		"function":  "StoreMessage",
		"prompt_id": m.Meta.AppData["prompt_id"],
	})
	log.Info()

	if m.Mime == entity.MimeBase64PNG {
		// If we succeeded in storing the image to persistent storage, we don't need
		// to include it in the data file.
		// Image resopnses are only written to the data file if they cannot be
		// decoded and written to an image file directly.
		if err := e.storeImage(ctx, m); err == nil {
			return nil
		}
	}

	bytes, _ := json.MarshalIndent(m, "", " ")
	if _, err := e.f.Write(append(bytes, '\n')); err != nil {
		log.Error(err)
		return fmt.Errorf("failed to write to data file '%s': %s", e.f.Name(), err)
	}
	return nil
}

func (e *StorageExternal) storeImage(ctx *app.Ctx, m *entity.Message) error {
	promptId := m.Meta.AppData["prompt_id"].(string)
	index := m.Meta.AppData["index"].(int)
	outOf := m.Meta.AppData["out_of"].(int)

	log := ctx.Log.WithFields(logger.Fields{
		"component": "StorageExternal",
		"function":  "storeImage",
		"prompt_id": promptId,
		"index":     index,
		"out_of":    outOf,
	})
	log.Info()

	file, err := os.OpenFile(
		path.Join(e.basedir, imageDir,
			fmt.Sprintf("%s--%d-of-%d.png", promptId, index+1, outOf)),
		os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		log.Error(err)
		return fmt.Errorf("storeImage: failed to open image file: %s", err)
	}
	defer file.Close()

	raw, err := base64.StdEncoding.DecodeString(m.Data)
	if err != nil {
		log.Error(err)
		return fmt.Errorf("storeImage: failed to decode bytes: %s", err)
	}

	_, err = file.Write(raw)
	if err != nil {
		log.Error(err)
		return fmt.Errorf("storeImage: failed to write image data: %s", err)
	}

	return nil
}
