<div style="display: flex; justify-content: space-between">

<div>

# Ada

</div>

<img src="assets/icon.ico" width="75px">

</div>

Ada is a GUI frontend for the [OpenAI API](https://platform.openai.com/docs/api-reference),
built using the [fyne](fyne.io) toolkit.

## Modes

Ada has two modes of operation
  * **Completion**: the user enters a prompt, and receives an AI completion back.
  * **Image generation**: user enters a prompt, and receives an AI generated image back.
    * Generated images are stored to an images data directory.
  * In both modes of operation, the conversation is written to a data file.

## Prompt

The prompt offers some commands to interact with the system.
  * `/quit` exits the application
  * `/back` or `/home` goes back to the home screen
  * `/clear` or `/cls` clears the screen
  * `/theme` toggles between light and dark mode

There are also commands which are used to interact with the state of the mode.
This state is usually the parameters passed to the OpenAI API.
  * **Completion mode commands**
    * `/set #tokens [num_tokens]`
  * **Image mode commands**
    * `/set n [n]` (`n` is the number of images to generate)
    * `/set res [one of: 256, 512, 1024]` (generated image resolution)

