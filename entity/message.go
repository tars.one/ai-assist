package entity

import "time"

type Mime uint16

const (
	MimeText Mime = iota
	MimeBase64PNG
)

type Metadata struct {
	CreatedAt time.Time              `json:"created_at,omitempty"`
	AppData   map[string]interface{} `json:"app_data,omitempty"`
}

type Message struct {
	Meta Metadata `json:"metadata,omitempty"`

	Kind string `json:"kind,omitempty"`
	Mime Mime   `json:"type,omitempty"`
	Data string `json:"data,omitempty"`
}
