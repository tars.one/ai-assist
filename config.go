package main

import (
	"git.tars.one/ada/app"
	"git.tars.one/ada/resource/openai"
	"git.tars.one/ada/ui"

	"git.tars.one/atlas/config"
)

func init() {
	env := config.EnvLoader{}
	config.Load(&app.Config, env)

	json, err := config.NewJSONLoaderFromFile(app.JSONConfigFile)
	if err != nil {
		panic(err)
	}
	defer json.Source.Close()

	config.Load(&app.Config, json)
	config.Load(&openai.Config, json)
	config.Load(&ui.Config, json)
}
