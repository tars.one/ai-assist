APP=ada
GO_FILES=$(shell find . -name '*.go')
DATA_DIR=${HOME}/.local/ada/data

$(APP): $(GO_FILES) go.mod go.sum Makefile
	go build -o $@

install: $(APP) ./assets/icon.ico
	mkdir -p ~/bin
	install $< ~/bin
	mkdir -p ${DATA_DIR}/{images,assets}
	cp ./assets/icon.ico ${DATA_DIR}/assets

.PHONY: install
