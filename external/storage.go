package external

import (
	"git.tars.one/ada/app"
	"git.tars.one/ada/entity"
)

type Storage interface {
	StoreMessage(ctx *app.Ctx, m *entity.Message) error
}
