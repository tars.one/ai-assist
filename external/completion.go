package external

import "git.tars.one/ada/app"

type UIExternal interface {
	PromptCompleter
	ImageGenerator
}

type CompletionRequest struct {
	Tokens int
	Prompt string
}

type PromptCompleter interface {
	// Complete processes the given CompletionRequest.
	Complete(ctx *app.Ctx, req CompletionRequest) (string, error)
}

type ImageSize int

const (
	ImgSize256 = iota
	ImgSize512
	ImgSize1024
)

type ImageRequest struct {
	Prompt string
	N      int // number of images to generate
	Size   ImageSize
}

type ImageGenerator interface {
	// Generate processes the given ImageRequest and returns the images generated.
	Generate(ctx *app.Ctx, req ImageRequest) ([]string, error)
}
