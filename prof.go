package main

import (
	"runtime"
	"time"

	"git.tars.one/ada/app"
	"git.tars.one/atlas/logger"
)

func LogMemUsage(log logger.Logger, m *runtime.MemStats) {
	bToMb := func(b uint64) uint64 { return b / 1024 / 1024 }

	runtime.ReadMemStats(m)
	log.WithFields(logger.Fields{
		"alloc":      bToMb(m.Alloc),
		"totalAlloc": bToMb(m.TotalAlloc),
		"sys":        bToMb(m.Sys),
		"num_gc":     m.NumGC,
	}).Info("memory snapshot")
}

func profiler(ctx *app.Ctx) {
	log := ctx.Log.WithFields(logger.Fields{
		"component": "_profiler",
	})

	var m runtime.MemStats
	t := time.NewTicker(15 * time.Second)

	LogMemUsage(log, &m)

	for {
		select {
		case <-t.C:
			LogMemUsage(log, &m)
		}
	}
}
